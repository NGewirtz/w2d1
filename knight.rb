require_relative 'stepable.rb'

class Knight < Piece

  Directions =  [
    [-2, -1],
    [-2,  1],
    [-1, -2],
    [-1,  2],
    [ 1, -2],
    [ 1,  2],
    [ 2, -1],
    [ 2,  1]
  ]
  include Stepable

  def inspect
    "k"
  end

  def get_moves
    moves(Directions, @board)
  end
end
