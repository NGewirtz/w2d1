require_relative 'stepable.rb'
# require_relative 'board.rb'

class King < Piece
  Directions = [[1,0], [1,1], [0,1], [-1,1], [0,-1], [-1,-1], [-1,0], [1,-1]]

  include Stepable

  def inspect
    "K"
  end

  def get_moves
    moves(Directions, @board)
  end


end
