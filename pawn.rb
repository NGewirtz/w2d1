# require_relative "null_piece.rb"
require_relative 'stepable.rb'

class Pawn < Piece
  include Stepable

  def initialize(pos, board)
    super
    @index_direction = pos[0] == 1 ? :up : :down
  end

  def inspect
    "b"
  end

  def forward_step
    if @index_direction == :up
      [[1,0]]
    else
      [[-1,0]]
    end
  end

  def side_attacks
    steps = []
    # debugger
    if @index_direction == :up && pos[0] < 7
      steps << [1, 1] if @board.grid[pos[0] + 1][pos[1] + 1] != NullPiece.instance
      steps << [1, -1] if @board.grid[pos[0] + 1][pos[1] - 1] != NullPiece.instance
    elsif pos[0] > 0
      steps << [- 1, 1] if @board.grid[pos[0] - 1][pos[1] + 1] != NullPiece.instance
      steps << [- 1, - 1] if @board.grid[pos[0] - 1][pos[1] - 1] != NullPiece.instance
    end
    steps
  end

  def get_moves()
    directions = forward_step + side_attacks
    moves(directions, @board)
  end

end
