require_relative 'piece.rb'
require_relative 'rook.rb'
require_relative 'king.rb'
require_relative 'knight.rb'
require_relative 'null_piece.rb'
require_relative 'pawn.rb'
require_relative 'queen.rb'
require_relative 'bishop.rb'
require 'byebug'
require_relative 'display.rb'
require_relative "human_player.rb"
# require_relative 'stepable.rb'

class Board

  attr_accessor :grid, :display, :player1, :player2

  def initialize
    #@grid = Array.new(8) { Array.new(8) { Piece.new } }
    @grid = build_grid
    @display = Display.new([4,4], self)
    @player1 = HumanPlayer.new("Gary", self)
    @player2 = HumanPlayer.new("Linda", self)
  end

  def build_grid
    [
      [Rook.new([0,0], self), Knight.new([0,1], self), Bishop.new([0,2], self), Queen.new([0,3], self), King.new([0,4], self), Bishop.new([0,5], self), Knight.new([0,6], self), Rook.new([0,7], self)],
      [Pawn.new([1,0], self), Pawn.new([1,1], self), Pawn.new([1,2], self), Pawn.new([1,3], self), Pawn.new([1,4], self), Pawn.new([1,5], self), Pawn.new([1,6], self), Pawn.new([1,7], self)],
      [NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance],
      [NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance],
      [NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance],
      [NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance, NullPiece.instance],
      [Pawn.new([6,0], self), Bishop.new([6,1], self), Pawn.new([6,2], self), Pawn.new([6,3], self), Pawn.new([6,4], self), Pawn.new([6,5], self), Pawn.new([6,6], self), Pawn.new([6,7], self)],
      [Rook.new([7,0], self), Knight.new([7,1], self), Bishop.new([7,2], self), Queen.new([7,3], self), King.new([7,4], self), Bishop.new([7,5], self), Knight.new([7,6], self), Rook.new([7,7], self)]
    ]
  end

  def get_piece
    hit_enter = false
    current = ""
    until hit_enter == :return
      system('clear')
      display.render(self)
      hit_enter = display.cursor.get_input
      current = display.cursor.cursor_pos.dup
      self[current].selected = true if hit_enter == :return
    end
    current
  end

  def get_destination
    hit_enter = false
    new_current = ""
    until hit_enter == :return
      system('clear')
      display.render(self)
      hit_enter = display.cursor.get_input
      new_current = display.cursor.cursor_pos.dup
    end
    grid.each do |row|
      row.each { |piece| piece.selected = false }
    end
    new_current
  end

  def move_piece(start_pos, end_pos)
  #   raise NoPieceError "I am here" if self[start_pos].class == NullPiece
  # rescue
  #   retry
    if self[start_pos].get_moves.include?(end_pos)
      self[start_pos], self[end_pos] = self[end_pos], self[start_pos]
      self[start_pos].board = self
      self[start_pos].pos = end_pos
      display.render(self)
    else
      player1.play_turn
    end
    # debugger
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, value)
    x, y = pos
    grid[x][y] = value
  end

  def take_input

  end
end

x = Board.new
# p x.grid[6][1].diagonal_dirs(x.grid[6][1].pos)
x.player1.play_turn




class NoPieceError < StandardError
  def message
    puts "Please select a piece"
  end
end
