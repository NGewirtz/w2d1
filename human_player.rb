class HumanPlayer
  def initialize(name, board)
    @name = name
    @board = board
  end

  def play_turn
    until false
      start_pos = @board.get_piece
      # debugger
      end_pos = @board.get_destination
      @board.move_piece(start_pos, end_pos)
    end
  end
end
