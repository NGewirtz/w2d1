module Slideable

  def moves

  end

  def move_dirs

  end

  def horizontal_dirs(start_pos)
    undiagonal_dirs = []

    (start_pos[0] - 1).downto(0) do |n|

      undiagonal_dirs << [n, start_pos[1]] unless board[[n, start_pos[1]]] != NullPiece.instance && board[[n, start_pos[1]]].color == board[start_pos].color
      break if board[[n, start_pos[1]]] != NullPiece.instance
    end

    (start_pos[0] + 1).upto(7) do |n|
      undiagonal_dirs << [n, start_pos[1]] unless board[[n, start_pos[1]]] != NullPiece.instance && board[[n, start_pos[1]]].color == board[start_pos].color
      break if board[[n, start_pos[1]]] != NullPiece.instance
    end

    (start_pos[1] - 1).downto(0) do |n|
      undiagonal_dirs << [start_pos[0], n] unless board[[start_pos[0], n]] != NullPiece.instance && board[[start_pos[0], n]].color == board[start_pos].color
      break if board[[start_pos[0], n]] != NullPiece.instance
    end

    (start_pos[1] + 1).downto(7) do |n|
      undiagonal_dirs << [start_pos[0], n] unless board[[start_pos[0], n]] != NullPiece.instance && board[[start_pos[0], n]].color == board[start_pos].color
      break if board[[start_pos[0], n]] != NullPiece.instance
    end

    undiagonal_dirs
  end

  def diagonal_dirs(start_pos)
    diagonal_dirs = []
    stop = false
    diags = [[1,1], [-1,-1], [1,-1], [-1,1]]

    diags.each do |x,y|
      temp_start_pos = start_pos.dup
      temp_start_pos[0],temp_start_pos[1] = temp_start_pos[0] + x, temp_start_pos[1] + y
      # debugger
      next if board[temp_start_pos].nil?

      while valid_move?(diagonal_dirs.last, temp_start_pos)
        diagonal_dirs << temp_start_pos.dup
        temp_start_pos[0],temp_start_pos[1] = temp_start_pos[0] + x, temp_start_pos[1] + y
      end

    end
    diagonal_dirs
  end

  def valid_move?(last_position, position)
    # debugger

    last_position ||= position
    return false unless (0..7).include?(position[0]) && (0..7).include?(position[1])

    return false if board[position] != NullPiece.instance && board[position].color == board[self.pos].color

    if board[position] != NullPiece.instance && board[position].color != board[self.pos].color && board[last_position].color == board[position].color
      return false
    end

    true
  end

end
