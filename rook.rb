require_relative "slideable.rb"

class Rook < Piece
  include Slideable

  def inspect
    "R"
  end

  def value
    "R"
  end

  def get_moves
    horizontal_dirs(self.pos)
  end

end
