class Employee
  attr_accessor :salary

  def initialize(name, title, salary, boss)
    @name = name
    @title = title
    @salary = salary
    @boss = boss
  end

  def bonus(multiplier)
    bonus = (employee_salary) * multiplier
  end

end

class Manager < Employee

  def initialize(name, title, salary, boss, employees)
    super(name, title, salary, boss)
    @employees = employees
  end

  def bonus(multiplier)
    bonus = @employees.reduce(0){ |accum, employee| accum + employee.salary} * multiplier
  end
end

p ted = Employee.new("ted", "sweeper", 200009, "Bobo")
sharon = Employee.new("sharon", "sweeper", 200009, "Bobo")
harold = Employee.new("harold", "sweeper", 200009, "Bobo")
bobo = Manager.new("Bobo", "boss", 200, "", [ted, sharon, harold])
p bobo.bonus(2)
