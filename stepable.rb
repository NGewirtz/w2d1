# require_relative "board.rb"
# require_relative "king.rb"

module Stepable

  def moves(directions, board)
    possible_moves = move_diffs(directions)

    possible_moves.select do |x, y|
      (0..7).include?(x) && (0..7).include?(y) && (board.grid[x][y] == NullPiece.instance)
    end
  end

  def move_diffs(directions)
    # [[1,0], [1,1], [0,1], [-1,1], [0,-1], [-1,-1], [-1,0], [1,-1]]
    possible_moves = []
    # debugger
    directions.each do |x, y|
      possible_moves << [@pos[0] + x, @pos[1] + y]
    end

    possible_moves
  end

end
