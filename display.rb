require 'colorize'
require_relative 'cursor'

class Display
  attr_accessor :cursor

  def initialize(cursor_pos, board)
    @cursor = Cursor.new(cursor_pos, board)
  end

  def render(board, selected = false)
    #
    # board[cursor.cursor_pos] = board[cursor.cursor_pos].inspect.colorize(:blue)
    #
    # print board.grid
    # p "/n"
    # puts board.grid

    board.grid.each_with_index do |row, i|
      new_row = row.map.with_index do |piece, j|
        if board[[i, j]].selected == true
            piece.inspect.colorize(:background => :red)
        elsif cursor.cursor_pos == [i, j]
            piece.inspect.colorize(:blue)
        else
          piece.inspect
        end
      end
      print new_row.join(" ")
      puts
    end
  end
end
