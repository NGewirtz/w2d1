class Queen < Piece
  def inspect
    "Q"
  end

  def get_moves
    moves = horizontal_dirs(self.pos) + diagonal_dirs(self.pos)
    moves
  end
end
