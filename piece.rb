class Piece

  attr_accessor :pos, :selected, :board, :color
  def initialize(pos, board)
    @pos = pos
    @board = board
    @selected = false
    @color = [0,1].include?(pos[0]) ? :black : :white
    @moves = []
  end

  def inspect
    "-"
  end

  def moves

  end

end
