require_relative 'slideable.rb'

class Bishop < Piece

  include Slideable
  def inspect
    "B"
  end

  def get_moves
    diagonal_dirs(self.pos)
  end

end
